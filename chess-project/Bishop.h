#pragma once
#include "Piece.h"
#include <math.h>
#include <iostream>
class Bishop : public Piece
{
public:
	Bishop(Piece::Color color, Position pos);

	virtual bool checkMove(const Position& pos);
	virtual bool _checkIfPieceInWay(const Position& pos);
	virtual void updatePiece();
};

