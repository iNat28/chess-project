#pragma once
#include "Piece.h"

class Piece;
class Rook : public Piece
{
public:
	Rook(Piece::Color color, Position pos);
	bool checkMove(const Position& pos);
private:
	virtual bool _checkIfPieceInWay(const Position& pos);
	virtual void updatePiece();

	char _direction;
};