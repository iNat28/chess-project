#include "Knight.h"
/*
Usage: knight c'tor
Input: color, pos.
Output: none.
*/
Knight::Knight(Piece::Color color, Position pos) : Piece(color, 'N', pos)
{
}
/*
Usage: this function checks if the knight can move somewhere
Input: position.
Output: bool.
*/
bool Knight::checkMove(const Position& pos)
{
	unsigned int srcX = this->_pos.getIntX();
	unsigned int srcY = this->_pos.getIntY();
	unsigned int dstX = pos.getIntX();
	unsigned int dstY = pos.getIntY();

	return (srcX + 2 == dstX && srcY + 1 == dstY) ||
		(srcX + 1 == dstX && srcY + 2 == dstY) ||
		(srcX - 2 == dstX && srcY - 1 == dstY) ||
		(srcX - 1 == dstX && srcY - 2 == dstY) ||
		(srcX - 2 == dstX && srcY + 1 == dstY) ||
		(srcX + 1 == dstX && srcY - 2 == dstY) ||
		(srcX + 2 == dstX && srcY - 1 == dstY) ||
		(srcX - 1 == dstX && srcY + 2 == dstY);
}
/*
Usage: the knight never has to check if there are pieces in the way.
Input: pos.
Output: bool.
*/
bool Knight::_checkIfPieceInWay(const Position& pos)
{
	return false;
}
/*
Usage: this function is not used for this piece.
Input:
Output:
*/
void Knight::updatePiece()
{
}