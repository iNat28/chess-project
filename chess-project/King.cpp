#include "King.h"
/*
Usage: king C'tor.
Input: color and position.
Output: none.
*/
King::King(Piece::Color color, Position pos) : Piece(color, 'K', pos)
{
}

/*
Usage: checks if the king move is valid.
Input: position.
Output: bool.
*/
bool King::checkMove(const Position& pos)
{
	return ((pos.getIntX() == this->_pos.getIntX() + 1 && pos.getIntY() == this->_pos.getIntY()) ||
		(pos.getIntX() == this->_pos.getIntX() && pos.getIntY() == this->_pos.getIntY() + 1) ||
		(pos.getIntX() == this->_pos.getIntX() - 1 && pos.getIntY() == this->_pos.getIntY()) ||
		(pos.getIntX() == this->_pos.getIntX() && pos.getIntY() == this->_pos.getIntY() - 1) ||
		(pos.getIntX() == this->_pos.getIntX() + 1 && pos.getIntY() == this->_pos.getIntY() + 1) ||
		(pos.getIntX() == this->_pos.getIntX() - 1 && pos.getIntY() == this->_pos.getIntY() - 1) ||
		(pos.getIntX() == this->_pos.getIntX() - 1 && pos.getIntY() == this->_pos.getIntY() + 1) ||
		(pos.getIntX() == this->_pos.getIntX() + 1 && pos.getIntY() == this->_pos.getIntY() - 1)) || this->_checkIfPieceInWay(pos);
}
/*
Usage: checks if the dst piece is occupied.
Input: position.
Output: bool.
*/
bool King::_checkIfPieceInWay(const Position& pos)
{
	return (Piece::checkIfPieceEmpty(pos) && Piece::board[pos.getIntX()][pos.getIntY()]->getColor() == this->getColor());
}

void King::updatePiece()
{
}
