#pragma once

class Position
{
public:
	Position();
	Position(char x, char y);
	Position(unsigned int x, unsigned int y);
	//~Position();
	
	char getCharX() const;
	char getCharY() const;

	unsigned int getIntX() const;
	unsigned int getIntY() const;

	static char switchX(unsigned int x);
	static unsigned int switchX(char x);
	static char switchY(unsigned int y);
	static unsigned int switchY(char y);

	bool operator==(Position other) const;
	Position* operator=(const Position& other);
private:
	char _x;
	char _y;
};

