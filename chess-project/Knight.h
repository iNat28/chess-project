#pragma once
#include "Piece.h"
class Knight : public Piece
{
public:
	Knight(Piece::Color color, Position pos);

	virtual bool checkMove(const Position& pos);
	virtual bool _checkIfPieceInWay(const Position& pos);
	virtual void updatePiece();
};