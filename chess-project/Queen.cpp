#include "Queen.h"

/*
Usage: Queen C'tor.
Input: color and position.
Output: none.
*/
//we build a rook and a bishop in an invalid place in order for the code not to add then to the board.
Queen::Queen(Piece::Color color, Position pos) : Piece(color, 'Q', pos), rook(color, Position((unsigned int)9, (unsigned int)9)), bishop(color, Position((unsigned int)9, (unsigned int)9))
{
	//then afterwards, we change their position to equal the queen's one.
	this->rook.setPos(pos);
	this->bishop.setPos(pos);
}
/*
Usage: function checks if the move is valid.
Input: position.
Output: bool.
*/
bool Queen::checkMove(const Position& pos)
{
	//uses the bishop and rook check move in order to see if the move is valid.
	return this->rook.checkMove(pos) || this->bishop.checkMove(pos);
}
/*
Usage: no real need for this function because the bishop and rook check this witht heir own function.
Input: position.
Output: bool.
*/
bool Queen::_checkIfPieceInWay(const Position& pos)
{
	return false;
}
/*
Usage: updates the positions of the "virtual" bishop and rook to be the same as the queen.
Input:
Output:
*/
void Queen::updatePiece()
{
	this->bishop.setPos(this->_pos);
	this->rook.setPos(this->_pos);
}
