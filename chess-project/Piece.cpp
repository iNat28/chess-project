#include "Piece.h"

//static values for the static vars.
Piece*** Piece::board = new Piece**[8];
std::vector<Piece*>* Piece::pieces = new std::vector<Piece*>[2];
bool Piece::turn = true; //it starts as the white's turn

/*
Usage: Piece C'tor.
Input: the board char - which piece it is. and its position.
Output: none.
*/
Piece::Piece(char boardChar, Position pos) : _boardChar(boardChar), _pos(pos)
{
	if (Piece::checkValidPos(pos))
	{
		Piece::board[this->_pos.getIntX()][this->_pos.getIntY()] = this;
	}
}

Piece::Piece(Piece::Color color, char boardChar, Position pos) : Piece(Piece::colorToBool(color) ? boardChar : boardChar + 32, pos)
{
}

/*
Usage: Piece D'tor.
Note: Does not delete any static variables
Input: none.
Output: none.
*/
Piece::~Piece()
{
}
/*
Usage: function returns the type of piece or # if no piece exists.
Input: none.
Output: char.
*/
char Piece::_getBoardChar() const
{
	return this->_boardChar;
}
/*
Usage: gets the piece's position.
Input: none.
Output: position.
*/
const Position& Piece::getPos() const
{
	return this->_pos;
}
/*
Usage: sets the piece's position.
Input: position.
Output: none.
*/
void Piece::setPos(Position pos)
{
	this->_pos = pos;
}
/*
Usage: initiates a vector and a board that store the pieces and makes it easier to get their position.
Input: none.
Output: none.
*/
void Piece::init()
{
	int i = 0;
	int j = 0;

	for (i = 0; i < 8; i++)
	{
		Piece::board[i] = new Piece*[8];
		//number of pieces available.
		for (j = 0; j < 8; j++)
		{
			//for now.
			Piece::board[i][j] = nullptr;
		}
	}
	
	Piece::pieces = new std::vector<Piece*>[2];
}
/*
Usage: deletes all of the pieces from the board and the vector.
Input: none.
Output: none.
*/
void Piece::terminate()
{
	unsigned int i = 0;
	unsigned int j = 0;

	for (i = 0; i < 8; i++)
	{
		delete[] Piece::board[i];
	}
	delete[] Piece::board;
	
	for (i = 0; i < 2; i++)
	{
		for (j = 0; j < Piece::pieces[i].size(); j++)
		{
			Piece::deletePiece(Piece::pieces[i][j]);
		}
	}
	delete[] Piece::pieces;
}
/*
Usage: gets the color of the piece.
Input: none.
Output: int.
*/
Piece::Color Piece::getColor() const
{
	return this->_boardChar >= 'A' && this->_boardChar <= 'Z' ? Piece::Color::WHITE : Piece::Color::BLACK;
}
bool Piece::getBoolColor() const
{
	return Piece::colorToBool(this->getColor());
}
/*
Usage: gets the right number for each scenario.
Input: complete position - src and dst.
Output: int.
*/
unsigned int Piece::getErrorMoveNumber(std::string frontEndPeicePos)
{
	Position srcPos = Position();
	Position dstPos = Position();
	Piece* srcPiece = nullptr;
	Piece* dstPiece = nullptr;
	//9 - used if the frontendpiece is smaller than 4
	//will deal with that later.
	unsigned int errorNum = 9;

	if (frontEndPeicePos.size() == 4)
	{
		//getting exact poistion.
		srcPos = Position(frontEndPeicePos[0], frontEndPeicePos[1]);
		dstPos = Position(frontEndPeicePos[2], frontEndPeicePos[3]);
		srcPiece = Piece::getPiece(srcPos);
		dstPiece = Piece::getPiece(dstPos);

		//getting the number according to the scenario
		//if the wrong turn is played
		if (srcPiece == nullptr || srcPiece->getBoolColor() != turn)
		{
			errorNum = 2;
		}
		//the src and dst positions are the same.
		else if (srcPos == dstPos)
		{
			errorNum = 7;
		}
		//the dst piece's color is the same and the mover.
		else if (dstPiece != nullptr && dstPiece->getColor() == srcPiece->getColor())
		{
			errorNum = 3;
		}
		//the dst or the src are out of range.
		else if (!(Piece::checkValidPos(srcPos) && Piece::checkValidPos(dstPos)))
		{
			errorNum = 5;
		}
		//if the piece can phisycally move to the dst position.
		else if (!srcPiece->checkMove(dstPos))
		{
			errorNum = 6;
		}
		//if valid whether regular or check.
		else
		{
			errorNum = 0;
			
			board[dstPos.getIntX()][dstPos.getIntY()] = srcPiece;
			srcPiece->setPos(dstPos);
			board[srcPos.getIntX()][srcPos.getIntY()] = nullptr;
			//eating a piece
			if (dstPiece != nullptr)
			{
				Piece::pieces[dstPiece->getColor()].erase(Piece::pieces[dstPiece->getColor()].begin() + Piece::getPieceIndex(dstPiece, dstPiece->getColor()));
			}
			//to check if the piece commits self check by moving
			if (srcPiece->_ifCommitsSelfCheck(dstPos))
			{
				//moving the piece to back.
				board[dstPos.getIntX()][dstPos.getIntY()] = dstPiece;
				srcPiece->setPos(srcPos);
				board[srcPos.getIntX()][srcPos.getIntY()] = srcPiece;
				//making sure not to add a null piece.
				if (dstPiece != nullptr)
				{
					Piece::pieces->push_back(dstPiece);
				}
				//the number for self check
				errorNum = 4;
			}
			//if doesnt commit self check
			else
			{
				//getting rid of the temp dstpiece
				if (dstPiece != nullptr)
				{
					delete dstPiece;
				}
				srcPiece->updatePiece();
				//checking for check on the opponent now.
				
				//the number for check.
				if (srcPiece->_ifCommitsCheck())
				{
					errorNum = 1;
				}
				//next turn.
				Piece::turn = !Piece::turn;
			}
		}
	}

	return errorNum;
}
/*
Usage: checks to see if the piece commits self check by moving.
Input: dstpos
Output: bool.
*/
bool Piece::_ifCommitsSelfCheck(Position dstPos)
{
	int i = 0;
	bool commitsSelfCheck = false;
	//checks if any of the opponent pieces commit check
	for (i = 0; i < Piece::pieces[!Piece::turn].size() && !commitsSelfCheck; i++)
	{
		commitsSelfCheck = Piece::pieces[!Piece::turn][i]->_ifCommitsCheck();
	}

	return commitsSelfCheck;
}
bool Piece::_ifCommitsCheck()
{
	return this->checkMove(Piece::getKing(this->getColor() == Piece::Color::BLACK ? Piece::Color::WHITE : Piece::Color::BLACK)->getPos());
}
/*
Usage: gets the string for the board
Input: none.
Output: none.
*/
void Piece::getBoardStr(char board[66])
{
	unsigned int i = 0;
	unsigned int j = 0;

	for (i = 0; i < 8; i++)
	{
		for (j = 0; j < 8; j++)
		{
			//Switched j with i because we stored it in reversed
			if (Piece::board[j][7 - i])
			{
				board[i * 8 + j] = Piece::board[j][7 - i]->_getBoardChar();
			}
			//if there is no piece
			else
			{
				board[i * 8 + j] = '#';
			}
		}
	}

	board[64] = '0';
	board[65] = '\0';
}
/*
Usage: gets the king
Input: color to determine which king to find.
Output: piece.
*/
Piece* Piece::getKing(Piece::Color color)
{
	//find the king in pieces vector and returns it.
	for (int i = 0; i < Piece::pieces[color].size(); i++)
	{
		if (Piece::colorToBool(color))
		{
			if (Piece::pieces[color][i]->_getBoardChar() == 'K')
			{
				return Piece::pieces[color][i];
			}
		}
		else
		{
			if (Piece::pieces[color][i]->_getBoardChar() == 'k')
			{
				return Piece::pieces[color][i];
			}
		}
	}
	return nullptr;
}
/*
Usage: checks if the piece is empty.
Input: position.
Output: bool.
*/
bool Piece::checkIfPieceEmpty(const Position& pos)
{
	return Piece::board[pos.getIntX()][pos.getIntY()];
}
/*
Usage: moves the piece.
Input: src, dst pieces and src, dst positions.
Output: none.
*/
void Piece::movePiece(Piece* srcPiece, const Position& srcPos, Piece* dstPiece, Position dstPos)
{
	board[dstPos.getIntX()][dstPos.getIntY()] = srcPiece;
	srcPiece->setPos(dstPos);
	board[srcPos.getIntX()][srcPos.getIntY()] = nullptr;
	deletePiece(dstPiece);
}
/*
Usage: gets the piece at a certain pos.
Input: position.
Output: piece.
*/
Piece* Piece::getPiece(const Position& pos)
{
	Piece* piece = nullptr;

	if (Piece::checkValidPos(pos))
	{
		piece = Piece::board[pos.getIntX()][pos.getIntY()];
	}

	return piece;
}
/*
Usage: checks if the position is valid.
Input: position.
Output: bool.
*/
bool Piece::checkValidPos(const Position& pos)
{
	return pos.getCharX() >= 'a' && pos.getCharX() <= 'h' && pos.getCharY() >= '1' && pos.getCharY() <= '8';
}
/*
Usage: deletes a piece.
Input: piece.
Output: none.
*/
void Piece::deletePiece(Piece* piece)
{
	if (piece != nullptr)
	{
		//erasing from the vector.
		Piece::pieces[piece->getColor()].erase(Piece::pieces[piece->getColor()].begin() + Piece::getPieceIndex(piece, piece->getColor()));
		delete piece;
	}
}
/*
Usage: gets the index of a piece in the pieces vector.
Input: piece, color.
Output: int.
*/
unsigned int Piece::getPieceIndex(Piece* piece, Piece::Color color)
{
	unsigned int i = 0;
	bool found = 0;
	//going through the vector.
	for (i = 0; i < Piece::pieces[color].size() && !found; i++)
	{
		found = Piece::pieces[color][i] == piece;
	}

	return --i;
}

bool Piece::colorToBool(Piece::Color color)
{
	return color == Piece::Color::WHITE;
}