/*
This file servers as an example of how to use Pipe.h file.
It is recommended to use the following code in your project, 
in order to read and write information from and to the Backend
*/

#include "Pipe.h"
#include "Communicator.h"
#include "Piece.h"
#include <iostream>
#include <thread>

int main()
{
	Pipe pipe = Pipe();
	bool isConnect = pipe.connect();
	char errNum[2] = { 0 };
	std::string ans;
	std::string msgFromGraphics = "";
	char initMsgToGraphics[66] = { 0 };
	
	while (!isConnect)
	{
		std::cout << "cant connect to graphics" << std::endl;
		std::cout << "Do you try to connect again or exit? (0-try again, 1-exit)" << std::endl;
		std::cin >> ans;

		if (ans == "0")
		{
			std::cout << "trying connect again.." << std::endl;
			Sleep(1000);
			isConnect = pipe.connect();
		}
		else 
		{
			pipe.close();
			return 0;
		}
	}

	Piece::init();
	Communicator::init();

	Piece::getBoardStr(initMsgToGraphics);
	pipe.sendMessageToGraphics(initMsgToGraphics);

	while (msgFromGraphics != "quit")
	{
		msgFromGraphics = pipe.getMessageFromGraphics();
		errNum[0] = Piece::getErrorMoveNumber(msgFromGraphics) + '0';
		pipe.sendMessageToGraphics(errNum);
	}

	pipe.close();
	Piece::terminate();
	
	return 0;
}