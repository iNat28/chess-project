#pragma once
#include "Piece.h"
class Piece;
class King : public Piece
{
public:
	King(Piece::Color color, Position pos);

	virtual bool checkMove(const Position& pos);
	virtual bool _checkIfPieceInWay(const Position& pos);
	virtual void updatePiece();
};

