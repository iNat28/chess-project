#include "Rook.h"
/*
Usage: rook c'tor.
Input: color and position.
Output: none.
*/
Rook::Rook(Piece::Color color, Position pos) : Piece(color, 'R', pos)
{
	this->_direction = '\0';
}

/*
Usage: checks if the rook move is valid.
Input: position.
Output: bool.
*/
bool Rook::checkMove(const Position& pos)
{
	//either on the same x
	if (this->_pos.getCharX() == pos.getCharX())
	{
		//checking whether the rook wants to move south, east, west or north. and for the chosen way checking if there is a piece in the way
		if (pos.getCharY() < this->_pos.getCharY())
		{
			this->_direction = 'n';
			return !this->_checkIfPieceInWay(pos);
		}
		else
		{
			this->_direction = 's';
			return !this->_checkIfPieceInWay(pos);
		}
	}
	//or the same y
	else if (this->_pos.getCharY() == pos.getCharY())
	{
		if (pos.getCharX() > this->_pos.getCharX())
		{
			this->_direction = 'e';
			return !this->_checkIfPieceInWay(pos);
		}
		else
		{
			this->_direction = 'w';
			return !this->_checkIfPieceInWay(pos);
		}
	}
	//if not the same x or the same y meaning invalid movement.
	else
	{
		return false;
	}
}
/*
Usage: checks for if a piece is int he way for either north, west, east or south.
Input: position.
Output: bool.
*/
bool Rook::_checkIfPieceInWay(const Position& pos)
{	
	int i = 0;

	switch (this->_direction)
	{
	case 'e':
		//each loop is until i get to the dst pos each time checking for a piece in that poistion.
		for (i = 1; i < pos.getIntX() - this->_pos.getIntX(); i++)
		{
			if (Piece::board[this->_pos.getIntX() + i][this->_pos.getIntY()])
			{
				return true;
			}
		}
		break;
	case 'w':
		for (i = 1; i < this->_pos.getIntX() - pos.getIntX(); i++)
		{
			if (Piece::board[this->_pos.getIntX() - i][this->_pos.getIntY()])
			{
				return true;
			}
		}
		break;
	case 'n':
		for (i = 1; i < this->_pos.getIntY() - pos.getIntY(); i++)
		{
			if (Piece::board[this->_pos.getIntX()][this->_pos.getIntY() - i])
			{
				return true;
			}
		}
		break;
	case 's':
 		for (i = 1; i < pos.getIntY() - this->_pos.getIntY(); i++)
		{
			if (Piece::board[this->_pos.getIntX()][this->_pos.getIntY() + i])
			{
				return true;
			}
		}
		break;
	}
	return false;
}

void Rook::updatePiece()
{
}
