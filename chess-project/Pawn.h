#pragma once
#include "Piece.h"
class Pawn : public Piece
{
public:
	Pawn(Piece::Color color, Position pos);

	virtual bool checkMove(const Position& pos);
	virtual bool _checkIfPieceInWay(const Position& pos);
	virtual void updatePiece();
private:
	bool _firstMove;
};

