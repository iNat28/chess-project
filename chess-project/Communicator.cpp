#include "Communicator.h"
/*
Usage: communicator C'tor
Input: none.
Output: none.
*/
Communicator::Communicator()
{
}
/*
Usage: communicator D'tor.
Input: none.
Output: none.
*/
Communicator::~Communicator()
{
}

/*
Usage: initiates the board.
Input: none.
Output: none.
*/
void Communicator::init()
{
	unsigned int i = 0;
	unsigned int j = 0;
	Piece::Color color = Communicator::intToColor(i);

	for (i = 0; i < 2; i++)
	{
		color = Communicator::intToColor(i);
		
		//For the kings for now
		//Later also for the queen
		Piece::pieces[i].push_back(new King(color, Position(4, Communicator::getGridPos(i))));
		/*
		[0] - 'e8'
		[1] - 'e1'
		*/
		Piece::pieces[i].push_back(new Queen(color, Position(3, Communicator::getGridPos(i))));
		
		//For the rook for now
		//Later also for the bishop and knight
		for (j = 0; j < 2; j++)
		{
			Piece::pieces[i].push_back(new Rook(color, Position(Communicator::getGridPos(j), Communicator::getGridPos(i))));
			/*
			[0] - 'a8',
			[0] - 'h8',
			[1] - 'a1',
			[1] - 'h1'
			*/
			Piece::pieces[i].push_back(new Knight(color, Position(Communicator::getGridPos(j, 6), Communicator::getGridPos(i))));

			Piece::pieces[i].push_back(new Bishop(color, Position(Communicator::getGridPos(j, 5), Communicator::getGridPos(i))));
		}
		
		for (j = 0; j < 8; j++)
		{
			Piece::pieces[i].push_back(new Pawn(color, Position(j, Communicator::getGridPos(i, 6))));
		}
	}
}

unsigned int Communicator::getGridPos(int ifTop, unsigned int top)
{
	return ifTop == 1 ? 7 - top : top;;
}

Piece::Color Communicator::intToColor(unsigned int color)
{
	return color == 0 ? Piece::Color::BLACK : Piece::Color::WHITE;
}