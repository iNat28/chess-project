#pragma once
#include "Piece.h"
#include "Rook.h"
#include "King.h"
#include "Pawn.h"
#include "Knight.h"
#include "Bishop.h"
#include "Pipe.h"
#include "Queen.h"
#include <iostream>

class Communicator
{
public:
	Communicator();
	~Communicator();

	//std::string getBoardInput();
	//void printOutput(unsigned int errorNum);
	static void init();
private:
	static unsigned int getGridPos(int ifTop, unsigned int top = 7);
	static Piece::Color intToColor(unsigned int color);
	//white = true, black = false
};