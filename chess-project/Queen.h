#pragma once
#include "Piece.h"
#include "Bishop.h"
#include "Rook.h"
class Queen : public Piece
{
public:
	Queen(Piece::Color color, Position pos);

	virtual bool checkMove(const Position& pos);
	virtual bool _checkIfPieceInWay(const Position& pos);
	virtual void updatePiece();
private:
	Rook rook;
	Bishop bishop;
};

