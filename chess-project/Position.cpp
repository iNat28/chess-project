#include "Position.h"
/*
Usage: position C'tor.
Input: none.
Output: none.
*/
Position::Position() : Position('\0', '\0')
{
}
/*
Usage: another c'tor.
Input: index.
Output: none.
*/
Position::Position(char x, char y) : _x(x), _y(y)
{
}
/*
Usage: another c'tor.
Input: int index.
Output: none.
*/
Position::Position(unsigned int x, unsigned int y) : _x(switchX(x)), _y(switchY(y))
{
}
/*
Usage: gets the char of the x index.
Input: none.
Output: char.
*/
char Position::getCharX() const
{
	return this->_x;
}
/*
Usage: gets the char of the y index.
Input: none.
Output: char.
*/
char Position::getCharY() const
{
	return this->_y;
}
/*
Usage: gets the int of the x index.
Input: none.
Output: int.
*/
unsigned int Position::getIntX() const
{
	return Position::switchX(this->_x);
}
/*
Usage: gets the int of the y index.
Input: none.
Output: int.
*/
unsigned int Position::getIntY() const
{
	return Position::switchY(this->_y);
}
/*
Usage: switches the x int to an char.
Input: int. 
Output: char.
*/
char Position::switchX(unsigned int x)
{
	return x + 'a';
}
/*
Usage: switches the x char to an int.
Input: char.
Output: int.
*/
unsigned int Position::switchX(char x)
{
	return x - 'a';
}
/*
Usage: switches the y int to an char.
Input: int.
Output: char.
*/
char Position::switchY(unsigned int y)
{
	return y + '1';
}
/*
Usage: switches the y char to an int.
Input: char.
Output: int.
*/
unsigned int Position::switchY(char y)
{
	return y - '1';
}
/*
Usage: == operator for the positions.
Input: position.
Output: bool.
*/
bool Position::operator==(Position other) const
{
	return this->_x == other.getCharX() && this->_y == other.getCharY();
}
/*
Usage: = operator for the positions.
Input: position.
Output: position.
*/
Position* Position::operator=(const Position& other)
{
	this->_x = other.getCharX();
	this->_y = other.getCharY();

	return this;
}