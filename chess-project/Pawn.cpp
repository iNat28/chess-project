#include "Pawn.h"
/*
Usage: pawn c'tor.
Input: color, position.
Output: none.
*/
Pawn::Pawn(Piece::Color color, Position pos) : Piece(color, 'P', pos), _firstMove(true)
{
}
/*
Usage: function checks if the piece can move to the dst pos.
Input: pos.
Output: bool.
*/
bool Pawn::checkMove(const Position& pos)
{
	bool pieceEmpty = Piece::getPiece(pos) == nullptr;

	return ((pieceEmpty && this->_pos.getCharX() == pos.getCharX()) || (!pieceEmpty && (this->_pos.getIntX() + 1 == pos.getIntX() || this->_pos.getIntX() - 1 == pos.getIntX()))) && !this->_checkIfPieceInWay(pos);
}
/*
Usage: checks if there is a piece in the way.
Input: pos.
Output: bool.
*/
bool Pawn::_checkIfPieceInWay(const Position& pos)
{
	int relativeMove = 0;
	bool checkIfPieceInWay = false;

	if (this->getBoolColor())
	{
		relativeMove++;
	}
	else
	{
		relativeMove--;
	}

	return !(this->_pos.getIntY() + relativeMove == pos.getIntY() || (this->_firstMove && this->_pos.getIntY() + relativeMove * 2 == pos.getIntY() && Piece::getPiece(Position(this->_pos.getIntX(), this->_pos.getIntY() + relativeMove)) == nullptr));
}
/*
Usage: makes sure to update the piece so that we know it's not his first movement.
Input: none.
Output: none.
*/
void Pawn::updatePiece()
{
	this->_firstMove = false;
}
