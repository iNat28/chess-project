#include "Bishop.h"
/*
Usage: Bishop C'tor.
Input: color and pos.
Output: none.
*/
Bishop::Bishop(Piece::Color color, Position pos) : Piece(color, 'B', pos)
{
}
/*
Usage: checks if the requested move is valid.
Input: position.
Output: bool.
*/
bool Bishop::checkMove(const Position& pos)
{
	//calling each function once instead of many times later in the code.
	int srcX = this->_pos.getIntX();
	int srcY = this->_pos.getIntY();
	int dstX = pos.getIntX();
	int dstY = pos.getIntY();
	//returning whether the dst pos is in a reachable position.
	return (abs(srcY - dstY) == abs(srcX - dstX) && !this->_checkIfPieceInWay(pos));
}
/*
Usage: function checks if there is a piece in the way.
Input: position.
Output: bool.
*/
bool Bishop::_checkIfPieceInWay(const Position& pos)
{
	//getting positions once instead of many times during the function.
	int srcX = this->_pos.getIntX();
	int srcY = this->_pos.getIntY();
	int dstX = pos.getIntX();
	int dstY = pos.getIntY();
	//if the piece is to move south east.
	if (dstX > srcX && dstY > srcY)
	{
		//searching every piece in the way.
		for (int i = 1; i < abs(srcX - dstX); i++)
		{
			if (Piece::getPiece(Position(this->_pos.getIntX() + i, this->_pos.getIntY() + i)))
			{
				return true;
			}
		}
	}
	//if the piece is to move south west.
	else if (dstX < srcX && dstY > srcY)
	{
		//searching every piece in the way.
		for (int i = 1; i < abs(srcX - dstX); i++)
		{
			if (Piece::getPiece(Position(this->_pos.getIntX() - i, this->_pos.getIntY() + i)))
			{
				return true;
			}
		}
	}
	//if the piece is to move north east.
	else if (dstX > srcX && dstY < srcY)
	{
		//searching every piece in the way.
		for (int i = 1; i < abs(srcX - dstX); i++)
		{
			if (Piece::getPiece(Position(this->_pos.getIntX() + i, this->_pos.getIntY() - i)))
			{
				return true;
			}
		}
	}
	//if the piece is to move north west.
	else
	{
		//searching every piece in the way.
		for (int i = 1; i < abs(srcX - dstX); i++)
		{
			if (Piece::getPiece(Position(this->_pos.getIntX() - i, this->_pos.getIntY() - i)))
			{
				return true;
			}
		}
	}
	return false;
}

void Bishop::updatePiece()
{
}
