#pragma once
#include <iostream>
#include "Position.h"
#include <vector>

class Piece
{
public:
	enum Color
	{
		BLACK = 0,
		WHITE
	};
	static std::vector<Piece*>* pieces;

	Piece(char boardChar, Position pos);
	Piece(Piece::Color color, char boardChar, Position pos);
	~Piece();

	//Static functions
	static void init();
	static void terminate();
	static unsigned int getErrorMoveNumber(std::string frontEndPeicePos);
	static void getBoardStr(char board[66]);

	//Non-static functions
	void setPos(Position pos);
	Piece::Color getColor() const; //Uses the board char to get the color
protected:
	static Piece*** board;
	Position _pos;

	//Static functions
	//Piece functions
	static Piece* getPiece(const Position& pos);
	static bool checkIfPieceEmpty(const Position& pos);

	//Non-static functions
	const Position& getPos() const;
	bool getBoolColor() const;

	virtual bool checkMove(const Position& pos) = 0;
private:
	static bool turn;
	char _boardChar;
	
	//Static Functions
	//Piece Functions
	static Piece* getKing(Piece::Color color);
	static void movePiece(Piece* srcPiece, const Position& srcPos, Piece* dstPiece, Position dstPos);
	static bool checkValidPos(const Position& pos);
	static void deletePiece(Piece* piece);
	static unsigned int getPieceIndex(Piece* piece, Piece::Color color);

	//Color Functions
	static bool colorToBool(Piece::Color color);
	
	//Non-static Functions
	//Functions for check
	bool _ifCommitsSelfCheck(Position dstPos);
	virtual bool _ifCommitsCheck();
	
	//Board Functions
	char _getBoardChar() const;

	//Pure functions
	virtual bool _checkIfPieceInWay(const Position& pos)  = 0;
	virtual void updatePiece() = 0;
};